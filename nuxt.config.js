const env = require("dotenv").config();

export default {
  env: {
    ...env.parsed,
  },
  head: {
    title: "Graph clustering on news articles",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'See news articles grouped in clusters with related news. Powered by Mikhail Tkachenko'
      }
    ],
    htmlAttrs: {
      lang: "en"
    },
  },

  plugins: [],

  components: true,
};
