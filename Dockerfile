FROM node:latest

# create destination directory
RUN mkdir -p /usr/src/o-nuxt
WORKDIR /usr/src/o-nuxt

# copy the app, note .dockerignore
COPY . .
RUN npm install

# build necessary, even if no static files are needed,
# since it builds the server as well
RUN npm run build

# expose 5000 on container
EXPOSE 5040

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=5040
